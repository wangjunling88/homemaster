package accessory

import "adai.design/homemaster/container/service"

type Accessory struct {
	UUID     string             `json:"aid"`
	Type     Type               `json:"type"`
	Services []*service.Service `json:"services"`
}

func New(typ Type, uuid string) *Accessory {
	acc := &Accessory{
		UUID:	uuid,
		Type:  typ,
		Services: []*service.Service{},
	}
	return acc
}

func (a *Accessory) AddService(s *service.Service, sid int) {
	s.SetId(sid)
	a.Services = append(a.Services, s)
}

func (a *Accessory) RemoveService(sid int) bool {
	var air *service.Service = nil
	for _, sev := range a.Services {
		if sev.ID == sid {
			air = sev
		}
	}

	if air == nil {
		return false
	}

	var services []*service.Service
	for _, sev := range a.Services {
		if sev != air {
			services = append(services, sev)
		}
	}
	a.Services = services
	return true
}

// 配件信息
type Info struct {
	Name 			string
	SerialNumber 	string
	Manufacturer 	string
	Model 			string
	FirmwareRevision	string
}

const InfoSevId = 0

func (a *Accessory) SetInfo(info Info) {
	sev := service.NewAccessoryInformation()

	sev.Name.SetValue(info.Name)
	sev.SerialNumber.SetValue(info.SerialNumber)
	sev.Model.SetValue(info.Model)
	sev.Manufacturer.SetValue(info.Manufacturer)
	sev.FirmwareRevision.SetValue(info.FirmwareRevision)
	a.AddService(sev.Service, InfoSevId)
}