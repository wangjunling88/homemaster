package service

const (
	TypeAccessoryInformation = "1" // 配件基本属性
	TypeAccessoryBridge      = "2" // 桥接服务
	TypeAccessoryDFU         = "3" // 固件升级

	TypeContactSensor  = "11" // 接触传感器: 门磁
	TypeHTSensor       = "12" // 湿度计
	TypeLightSensor    = "13" // 光线传感器
	TypeOutlet         = "14" // 插座
	TypeSwitch         = "15" // 开关
	TypeAirConditioner = "16" // 空调
	TypeClock          = "17" // 闹钟
	TypeMotion         = "21" // 人体运动检测

	TypeLightColor            = "18" // 彩色灯
	TypeLightColorTemperature = "19" // 色温灯

	TypeZigbeeCoordinator = "20" // Zigbee协调器
)
