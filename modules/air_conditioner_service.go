package modules

import (
	"adai.design/homemaster/container"
	"adai.design/homemaster/container/characteristic"
	"adai.design/homemaster/container/characteristic/to"
	"adai.design/homemaster/container/service"
	"github.com/gorilla/websocket"
)

const AirConditionerSevId = 100

var airConditioner = service.NewAirConditioner()
var haier = &haierAirConditioner{
	Mode:        0x00,
	Temperature: 25,
	OnOff:       false,
}

func init() {
	container.AddService(airConditioner.Service, AirConditionerSevId)
	airConditioner.SetName("Haier")

	// 开关变化
	airConditioner.On.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		airConditioner.On.UpdateValue(newValue)
		if newValue == 1 {
			haier.OnOff = true
		} else {
			haier.OnOff = false
		}
		data, err := haier.encode()
		if err == nil {
			msg := &MasterMessage{
				pkgType: MasterInfrared,
				data:    data,
			}
			masterSendMessage(msg)
		}
	})

	// 温度控制
	airConditioner.Temperature.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		airConditioner.Temperature.UpdateValue(newValue)
		temperature := to.Int64(newValue)
		haier.Temperature = int(temperature)
		data, err := haier.encode()
		if err == nil {
			msg := &MasterMessage{
				pkgType: MasterInfrared,
				data:    data,
			}
			masterSendMessage(msg)
		}
	})

	// 模式
	airConditioner.Model.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		airConditioner.Model.UpdateValue(newValue)
		if newValue == characteristic.AirConditionerModeCool {
			haier.Mode = haierModeCool
		} else if newValue == characteristic.AirConditionerModeHeat {
			haier.Mode = haierModeHeat
		} else if newValue == characteristic.AirConditionerModeDehumidification {
			haier.Mode = haierModeDehumidification
		}
		data, err := haier.encode()
		if err == nil {
			msg := &MasterMessage{
				pkgType: MasterInfrared,
				data:    data,
			}
			masterSendMessage(msg)
		}
	})

	// 风速
	airConditioner.WindSpeed.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		airConditioner.WindSpeed.UpdateValue(newValue)
		windSpeed := to.Int64(newValue)
		if windSpeed >= 0 && windSpeed <= 25 {
			haier.WindSpeed = haierWindSpeedLevel1
		} else if windSpeed <= 50 {
			haier.WindSpeed = haierWindSpeedLevel2
		} else if windSpeed <= 75 {
			haier.WindSpeed = haierWindSpeedLevel3
		} else {
			haier.WindSpeed = haierWindSpeedAuto
		}
		data, err := haier.encode()
		if err == nil {
			msg := &MasterMessage{
				pkgType: MasterInfrared,
				data:    data,
			}
			masterSendMessage(msg)
		}
	})

	// 风模式
	airConditioner.RotationMode.OnValueUpdateFromConn(func(conn *websocket.Conn, c *characteristic.Characteristic, newValue, oldValue interface{}) {
		airConditioner.RotationMode.UpdateValue(newValue)

		rotationMode := to.Int64(newValue)
		if rotationMode == characteristic.AirConditionerRotationModeSwing {
			haier.RotationMode = haierWindDirectSwing
		} else if rotationMode == characteristic.AirConditionerWindDirectAbove {
			haier.RotationMode = haierWindDirectAbove
		} else if rotationMode == characteristic.AirConditionerWindDirectMiddle {
			haier.RotationMode = haierWindDirectMiddle
		} else if rotationMode == characteristic.AirConditionerWindDirectLower {
			haier.RotationMode = haierWindDirectLower
		}
		data, err := haier.encode()
		if err == nil {
			msg := &MasterMessage{
				pkgType: MasterInfrared,
				data:    data,
			}
			masterSendMessage(msg)
		}
	})
}
