package coordinator

/*

获取版本
20:53:24.493 -> 01 02 10 10 02 10 02 10 10 03
// real: 01 00 10 00 00 10 03
20:53:24.539 <- 01 80 00 00 04 94 00 00 00 10 03
20:53:24.619 <- 01 80 10 00 04 9C 12 34 56 78 03


Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0x00
  Message:
Type: 0x8010
 (Version)
  Length: 4
  Application: 4660
  SDK: 22136

开启网络
20:53:57.735 -> 01 02 10 24 02 10 02 10 24 03
real: 01 00 24 00 00 24 03

20:53:57.766 <- 01 80 24 00 01 A1 04 03
20:53:57.801 <- 01 80 00 00 04 A0 00 00 00 24 03


Type: 0x8024
 (Network Up)
  Status: 0x04
  Short Address: 0x0000
  Extended Address: 0x2400000000000000
  Channel: 0
Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0x00
  Message:


获取网络状态
20:54:41.516 -> 01 02 10 02 19 02 10 02 10 02 19 03
real: 01 00 09 00 00 09 03
20:54:41.583 <- 01 80 00 00 04 8D 00 00 00 09 03
20:54:41.615 <- 01 80 09 00 15 84 00 00 00 15 8D 00 02 75 67 C9 F2 BD 21 A6 1A 3A 7B 90 E4 AA 14 03

Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0x00
  Message:
Type: 0x8009
 (Network State Response)
  Short Address: 0x0000
  Extended Address: 0x00158D00027567C9
  PAN ID: F2BD
  Ext PAN ID: 0x21A61A3A7B90E4AA
  Channel: 20

发现设备
20:55:12.951 -> 01 02 10 4E 02 10 02 13 4D 02 10 02 10 02 10 03
// 01 00 4e 00 03 4d 00 00 00 03
20:55:13.021 <- 01 80 01 00 17 A6 06 4D 61 6E 61 67 65 6D 65 6E 74 20 4C 71 69 20 52 65 71 75 65 73 74 03
20:55:13.084 <- 01 80 00 00 04 78 00 B2 00 4E 03
20:55:13.119 <- 01 80 4E 00 2F 4B B2 00 02 02 00 53 73 21 A6 1A 3A 7B 90 E4 AA 00 15 8D 00 02 75 67 E5 00 D6 69 82 F9 21 A6 1A 3A 7B 90 E4 AA 00 15 8D 00 02 75 67 DC 00 AC 69 03

Type: 0x8001
 (Log)
  Level: 0x06
  Message: Management Lqi Request
Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0xB2
  Message: Na
Type: 0x804E
 (Mgmt LQI Response)
  SQN: 0xB2
  Status: 0x00
  Nb Table Entries: 2
  Start Index: 0
  Nb Table List Count: 2
  Neighbor 0:
    Extended Pan ID: 0x21A61A3A7B90E4AA
    Extended Address: 0x158D00027567E5
    Nwk Address: 0x5373
    LQI: 214
    Depth: 0
    Flags: 0x69
    Device Type: Router
    Permit Joining: Unknown
    Relationship: Sibling
    RxOnWhenIdle: Yes
  Neighbor 1:
    Extended Pan ID: 0x21A61A3A7B90E4AA
    Extended Address: 0x158D00027567DC
    Nwk Address: 0x82F9
    LQI: 172
    Depth: 0
    Flags: 0x69
    Device Type: Router
    Permit Joining: Unknown
    Relationship: Sibling
    RxOnWhenIdle: Yes

开灯
20:56:48.885 -> 01 02 10 92 02 10 02 16 B7 02 12 53 73 02 11 02 11 02 11 03
20:56:48.963 <- 01 80 00 00 04 15 00 03 00 92 03
20:56:49.048 <- 01 81 01 00 09 AD 03 53 73 01 01 00 06 01 00 03

Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0x03
  Message:
Type: 0x8101
 (Default Response)
  SQN: 0x03
  Short Address: 0x5373
  Source EndPoint: 0x7B
  Destination EndPoint: 0x90
  Cluster ID: 0xE4AA (Unknown)
  Command: 0x00
  Status: 0x15

关灯
20:57:20.202 -> 01 02 10 92 02 10 02 16 B6 02 12 53 73 02 11 02 11 02 10 03
real: 01 00 92 00 06 B6 02 53 73 01 01 00 03

20:57:20.280 <- 01 80 00 00 04 13 00 05 00 92 03
20:57:20.356 <- 01 81 01 00 09 AA 05 53 73 01 01 00 06 00 00 03

Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0x05
  Message:
Type: 0x8101
 (Default Response)
  SQN: 0x05
  Short Address: 0x5373
  Source EndPoint: 0x7B
  Destination EndPoint: 0x90
  Cluster ID: 0xE4AA (Unknown)
  Command: 0x00
  Status: 0x15

获取设备信息
20:57:53.511 -> 01 02 10 45 02 10 02 12 67 53 73 03
20:57:53.549 <- 01 80 00 00 04 75 00 B4 00 45 03
20:57:53.596 <- 01 80 45 00 07 57 B4 00 53 73 02 01 02 03

Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0xB4
  Message: E
Type: 0x8045
 (Active Endpoints Response)
  SQN: 0xB4
  Status: 0x00
  Short Address: 0x5373
  Endpoint Count: 2
  Endpoint List:
    Endpoint 0: 0x01
    Endpoint 1: 0x02

获取设备应用信息
20:58:37.856 -> 01 02 10 43 02 10 02 13 61 53 73 02 11 03
20:58:37.909 <- 01 80 00 00 04 70 00 B7 00 43 03
20:58:37.972 <- 01 80 43 00 1D 58 B7 00 53 73 10 01 01 04 00 00 01 04 00 00 00 04 00 03 00 06 00 00 00 00 04 00 03 00 06 03

Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0xB7
  Message: C
Type: 0x8043
 (Simple Descriptor Response)
  SQN: 0xB7
  Status: 0x00
  Short Address: 0x5373
  Length: 16
  EndPoint: 0x01
  Profile ID: 0x0104 (ZigBee HA)
  Device ID: 0x0000 (Generic - On/Off Switch)
  Input Cluster Count: 4
    Cluster 0:  Cluster ID: 0x0000 (General: Basic)
    Cluster 1:  Cluster ID: 0x0004 (General: Groups)
    Cluster 2:  Cluster ID: 0x0003 (General: Identify)
    Cluster 3:  Cluster ID: 0x0006 (General: On/Off)
  Output Cluster Count: 0

读取开关属性
21:00:57.971 -> 01 02 11 02 10 02 10 02 1E 02 1D 02 12 53 73 02 11 02 11 02 10 02 16 02 10 02 10 10 37 02 11 02 10 02 10 03
21:00:58.090 <- 01 80 00 00 04 83 00 06 01 00 03
21:00:58.131 <- 01 81 00 00 0D BC 06 53 73 01 00 06 00 00 00 10 00 01 00 03

Type: 0x8000
 (Status)
  Length: 4
  Status: 0x00 (Success)
  SQN: 0x06
  Message: 
Type: 0x8100
 (Read Attrib Response)
  SQN: 0x06
  Src Addr: 0x5373
  EndPoint: 0x01
  Status: 0x00
  Cluster ID: 0x0006 (General: On/Off)
  Attribute ID: 0x0000
  Attribute Size: 0x0001
  Attribute Type: 0x10 (Boolean)
  Attribute Data: 0x00

 */
