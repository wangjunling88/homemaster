package client

import (
	"adai.design/homemaster/container"
	"encoding/json"
	"adai.design/homemaster/log"
	"time"
)

var messageHandleMap = map[string]Handler{}

type Handler interface {
	handle(client *Client, msg *Message) error
}

type Characteristic struct {
	AccessoryID 	string		`json:"aid"`
	ServiceId 		int			`json:"sid"`
	CharacteristicId  int 		`json:"cid"`
	Value 	 	interface{}		`json:"value"`
}

type CharacteristicController struct {
}

func (c *CharacteristicController) handle(client *Client, msg *Message) error {
	client.notify = notifyEnable
	// 获取所有属性的状态
	if msg.Method == MsgMethodGet {
		chars := make([]*Characteristic, 0)
		accessories := container.GetContainer().Accessories
		for _, a := range accessories {
			for _, s := range a.Services {
				for _, c := range s.Characteristics {
					char := &Characteristic{
						AccessoryID: a.UUID,
						ServiceId: s.ID,
						CharacteristicId: c.Id,
						Value: c.Value,
					}
					chars = append(chars, char)
				}
			}
		}

		data, _ := json.Marshal(chars)
		result := &Message{
			Path: msg.Path,
			Method: msg.Method,
			Data: data,
			State: "ok",
		}
		return client.writeMessage(result)
	}

	// 控制设备属性
	if msg.Method == MsgMethodPost {
		var chars []*Characteristic
		err := json.Unmarshal(msg.Data, &chars)
		if err != nil {
			log.Error("err: %v", err)
			return err
		}

		accessories := container.GetContainer().Accessories
		for _, char := range chars {
			for _, a := range accessories {
				if a.UUID == char.AccessoryID {
					for _, s := range a.Services {
						if s.ID == char.ServiceId {
							for _, c := range s.Characteristics {
								if c.Id == char.CharacteristicId {
									//log.Debug("aid(%s) value(%v) type(%v)", char.AccessoryID, char.Value, reflect.TypeOf(char.Value))
									c.UpdateValueFromConnection(char.Value, client.conn)
								}
							}
						}
					}
				}
			}
		}
		return nil
	}
	return nil
}

func init() {
	messageHandleMap[MsgPathCharacteristic] = &CharacteristicController{}
}

// 配件处理
type AccessoryController struct {
}

func (c *AccessoryController) handle(client *Client, msg *Message) error {
	client.notify = notifyEnable
	if msg.Method == MsgMethodGet {
		accessories := container.GetContainer()
		data, _ := json.Marshal(accessories)
		result := &Message{
			Path: msg.Path,
			Method: msg.Method,
			Data: data,
			State: "ok",
		}
		return client.writeMessage(result)
	}
	return nil
}

func init() {
	messageHandleMap[MsgPathAccessories] = &AccessoryController{}
}

// Container处理
type ContainerController struct {}

func (c *ContainerController) handle(client *Client, msg *Message) error {
	info := map[string]interface{}{
		"version": container.GetContainer().Version,
		"firmware": "0.0.1",
		"date": time.Now().Add(time.Hour * 8).Format("2006/01/02 15:04:05"),
	}

	if msg.Method == MsgMethodGet {
		data, _ := json.Marshal(info)
		result := &Message{
			Path: msg.Path,
			Method: MsgMethodGet,
			Data: data,
			State: "ok",
		}
		return client.writeMessage(result)
	}
	return nil
}

func init() {
	messageHandleMap[MsgPathContainer] = &ContainerController{}
}

